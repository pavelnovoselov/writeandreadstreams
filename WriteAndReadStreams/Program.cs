﻿using System;
using System.Threading;


namespace WriteAndReadStreams
{
    internal class Program
    {
        static object blockWrite = new object();
        static Semaphore sRead = new Semaphore(2, 2);
        static EventWaitHandle handleRead = new AutoResetEvent(false);
        static EventWaitHandle handleWrite = new AutoResetEvent(false);

        static void Read(object obj)
        {
            int num = (int)obj;

            sRead.WaitOne();
            var startTime = System.Diagnostics.Stopwatch.StartNew();
            Thread.Sleep(100);
            startTime.Stop();
            var resultTime = startTime.Elapsed;
            string elapsedTime = String.Format("{2:00}.{3:000}",
                resultTime.Hours,
                resultTime.Minutes,
                resultTime.Seconds,
                resultTime.Milliseconds);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"Блок {num} прочитан {elapsedTime} mc");

            handleWrite.Set();
            handleRead.WaitOne();
            sRead.Release();
        }

        static void Write(object obj)
        {
            int num = (int)obj;

            lock (blockWrite)
            {
                handleWrite.WaitOne();
                var startTime = System.Diagnostics.Stopwatch.StartNew();
                Thread.Sleep(200);
                startTime.Stop();
                var resultTime = startTime.Elapsed;
                string elapsedTime = String.Format("{2:00}.{3:000}",
                    resultTime.Hours,
                    resultTime.Minutes,
                    resultTime.Seconds,
                    resultTime.Milliseconds);
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"                  Блок {num} Записан {elapsedTime} mc");

                handleRead.Set();
            }
        }

        static void Main()
        {
            for (int i = 1; i <= 10; i++)
            {
                Thread read = new Thread(Read);
                Thread write = new Thread(Write);

                read.Start(i);
                write.Start(i);
            }
            Console.ReadLine();
        }
    }
}
